public class LZWTree
{
	public Node tree = new Node();
	int Insert(String Token, int index, Node node)
	{
		if (Token.length() > index)
		{
			char relevantBool = Token.charAt(index);
			if (relevantBool == '0')
			{
				if(node.left== null)
				{
					node.makeLeft();
					node.left.data = relevantBool;
					return index+1;
				}
				else
				return Insert(Token, index+1,node.left);
				
			}
			else
			{
				if(node.right== null)
				{
					node.makeRight();
					node.left.data = relevantBool;
					return index+1;
				}
				else
					return Insert(Token, index+1,node.right);
				
			}
		}else
			return Token.length() + 1;
	}
	public void fill(String input)
	{
		int StrIndex = 0;
		while(input.length()>StrIndex)
			StrIndex = Insert(input, StrIndex, tree);
	}
}


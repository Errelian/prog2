package backend;
import backend.Node;
@SuppressWarnings("unused")
public class LZWTree
{
	public Node tree = new Node();
	int Insert(String Token, int index, Node node)
	{
		if (Token.length() > index)
		{
			char relevantBool = Token.charAt(index);
			
			if (relevantBool == '0')
			{
				if(node.left== null)
				{
					node.makeLeft();
					node.left.data = relevantBool;
					return index+1;
				}
				else
				return Insert(Token, index+1,node.left);
				
			}
			else
			{
				if(node.right== null)
				{
					node.makeRight();
					node.right.data = relevantBool;
					return index+1;
				}
				else
					return Insert(Token, index+1,node.right);
				
			}
		}else
			return Token.length() + 1;
	}
	public void fill(String input)
	{
		int StrIndex = 0;
		
		while(input.length() > StrIndex)
			StrIndex = Insert(input, StrIndex, tree);
	}
	public String postFixPrint(Node tree)
	{
		String str =new String();
		str = "";
		if (tree != null)
		{		
			if (tree.left != null)
			{
				postFixPrint(tree.left);
			}
				
			if (tree.right != null)
			{
				postFixPrint(tree.right);
			}
			if(tree.data != 'p')
			{
				//System.out.print(tree.data);
				String asd= new String();
				asd = String.valueOf(tree.data);
				str += asd;
			}
				
		}
		else
			return null;
		//System.out.print(str);
		return str;
	}
}


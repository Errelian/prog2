public class Node {
	char data = '/';
	Node left;
	Node right;
	Node parent;
	public Node makeLeft()
	{
		if (left != null)
			return null;
		else 
			left = new Node();
		left.parent = this;
		return left;
	}
	public Node makeRight()
	{
		if (right != null)
			return null;
		else 
			right = new Node();
		right.parent = this;
		return right;
	}
}

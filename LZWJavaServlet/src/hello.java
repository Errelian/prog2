import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import backend.LZWTree;
@WebServlet("/Hello!")
public class hello extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String inStr = request.getQueryString();
		
		if(inStr == null)
		{
			response.getWriter().append("Write something in the Get Query\r\n");
		}else
		{
			LZWTree asd = new LZWTree();
			System.out.print("In String: "+inStr+"\n");
			response.getWriter().print("In String: "+inStr+"\r\n");
			asd.fill(inStr);
			System.out.print("Prefix: \r\n");
			String das = new String();
			das += asd.postFixPrint(asd.tree);
			System.out.print(das);
			response.getWriter().print("Prefix: \r\n");
			response.getWriter().print(das);
		}
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
}
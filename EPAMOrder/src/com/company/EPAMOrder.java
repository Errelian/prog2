package com.company;
import java.util.*;

public class EPAMOrder   {
    public static <T extends Comparable<T>> List createOrderedList(Collection<T> input)
    {
        List asd = Arrays.asList(input.toArray());
        Collections.sort(asd);
        return asd;
    }
    public static void test()
    {
        ArrayList<Integer> das = new ArrayList<>(5);
        das.add(5);
        das.add(9);
        das.add(4);
        das.add(0);
        das.add(3);
        List<Integer> dass = createOrderedList(das);

        for(Integer elem : das)
            System.out.println(elem);

        for(Integer elem : dass)
            System.out.println(elem);

        ArrayList<Wrapper1> asd3 = new ArrayList<>(3);
        asd3.add(new Wrapper1(5));
        asd3.add(new Wrapper1(0));
        asd3.add(new Wrapper1(9));
        asd3.add(new Wrapper1(3));

        for(Wrapper1 elem : asd3)
            System.out.println(elem.number);
        //List<Wrapper1> asd4 = createOrderedList(asd3);

        //for(Wrapper1 elem : asd4)
        //	System.out.println(elem.anInt); //Mivel a Wrapper1 osztály nem implementálja a Comparable<Wrapper1> interfészt, ezért ez compile time errort dobna.

        ArrayList<Wrapper2> asd5 = new ArrayList<>(3);
        asd5.add(new Wrapper2(5));
        asd5.add(new Wrapper2(0));
        asd5.add(new Wrapper2(9));
        asd5.add(new Wrapper2(3));

        List<Wrapper2> asd6 = createOrderedList(asd5);

        for(Wrapper2 elem : asd5)
            System.out.println(elem.number);

        for(Wrapper2 elem : asd6)
            System.out.println(elem.number);
    }

}
class Wrapper1
{
    int number;

    public Wrapper1(int numeral) {number = numeral;}
}


class Wrapper2 implements Comparable<Wrapper2>
{
    int number;

    public Wrapper2(int numeral) {number = numeral;}

    @Override
    public int compareTo(Wrapper2 ojjektum) {
        return number-ojjektum.number;
    }
}
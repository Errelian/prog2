public class Gagyi
{
    public static void rossz()
    {
        MyInteger a = MyInteger.valueOf(17);
        MyInteger b = MyInteger.valueOf(17);

        System.out.println("a: " +a);
        System.out.println("b: " +b);

        System.out.println("a <= b: " +a.smallerI(b));
        System.out.println("a >= b: " +a.largerI(b));
        System.out.println("a == b: " +(b == a)+"\n");

        a = MyInteger.valueOf(132);
        b = MyInteger.valueOf(132);

        System.out.println("a: "+a);
        System.out.println("b: "+b);

        System.out.println("a <= b: "+a.smallerI(b));
        System.out.println("a >= b: "+a.largerI(b));
        System.out.println("a == b: "+(b == a));
        System.out.println("a.equals(b): "+a.equals(b));
    }
}
class MyInteger
{
    int value;

    MyInteger(int i)
    {
        value = i;
    }
    public static MyInteger valueOf(int i)
    {
        final int offset = 128;
        if (i >=-128 && i<=127)
            return MyIntegerCache.cache[i + offset];
        return new MyInteger(i);
    }
    private static class MyIntegerCache
    {
        private MyIntegerCache(){}
        static final MyInteger cache[] = new MyInteger[-(-128) + 127 +1];
        static {
            for(int i = 0; i <cache.length; i++)
                cache[i] = new MyInteger(i - 128);
        }
    }
    public int compare(MyInteger other)
    {
        return value - other.value;
    }
    public boolean larger(MyInteger other)
    {
        return this.compare(other) > 0;
    }

    public boolean smaller(MyInteger other)
    {
        return this.compare(other) < 0;
    }

    public boolean largerI(MyInteger other)
    {
        return this.compare(other) >= 0;
    }

    public boolean smallerI(MyInteger other)
    {
        return this.compare(other) <= 0;
    }

    public boolean equal(MyInteger other)
    {
        return this.compare(other) == 0;
    }
    @Override
    public String toString() {
        return Integer.toString(value);
    }

    @Override
    public boolean equals(Object obj) {
        MyInteger other = (MyInteger) obj;
        if (other == null)
            return false;
        else
            return other.value == this.value;
    }
}
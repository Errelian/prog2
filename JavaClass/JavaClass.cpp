#include <string>
#include <iostream>
#include <filesystem>
namespace fs = std::filesystem;
void JavaLister() {
    std::string pathSTR = "insert filepath here";
    fs::path path = pathSTR;
    for (const auto & entry : fs::recursive_directory_iterator(path))
        if (fs::path(entry.path()).extension() == ".java" &&fs::is_regular_file(entry.path()))
        {
            ClassNumber++;
            std::cout << entry.path() << std::endl;
        }
}
int main()
{
    JavaLister();
    return 0;
}
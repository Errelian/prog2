#ifndef CALLOC_CALLOC_H
#define CALLOC_CALLOC_H

#endif //CALLOC_CALLOC_H

#pragma once

#include <cstdlib>
#include <new>
#include <limits>
#include <iostream>
#include <vector>

template <typename T>
struct Reporter
{
    T data{};

    Reporter()
    {
        std::cout << "Constructed " << data << "\n";
    }

    Reporter(T in) : data(in)
    {
        std::cout << "Constructed " << data << "\n";
    }


    ~Reporter()
    {
        std::cout << "Destructed " << data << "\n";
    }
};

template <typename T>
struct CustomAlloc{
    typedef T value_type;

    CustomAlloc()= default;
    CustomAlloc(const CustomAlloc &)= default;
    ~CustomAlloc()= default;

    T* allocate(std::size_t i)
    {
        if (i > std::numeric_limits<std::size_t>::max() / sizeOf(T))
            throw std::bad_alloc();
        if(auto p = static_cast<T*>(std::malloc(n + sizeof(T))))
        {
            report(p,i);
            return p;
        }

        throw std::bad_alloc();
    }
private:
    void report(T* p, std::size_t i, bool alloc = true) const
    {
        std::cout <<(alloc ? "Alloc:" : "Dealloc: ") <<sizeof(T)* i<<  "bytes at "
        << std::hex << std::showbase << reinterpret_cast<void*>(p) << std::dec << "\n";
    }
};
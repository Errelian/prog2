package com.company;
public class Parent
{
    String a = "";
    String name()
    {
        return "Parent";
    }
    public void test()
    {
        Parent parent = new Parent();
        Child child = new Child();

        System.out.println(parent.name());
        System.out.println(child.name());
        //System.out.println(child.age()); //would throw an error 'cause Parent has no member called "age()"
    }
}

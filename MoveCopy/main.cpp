// a program a Prog1-es LZW fa egy részlete, ez a részlet természetesen nem fordul le csak demonstrációs céllal van itt.
Node * cp(Node *node, Node *treep)      //lemásoljuk magát az egész fát, Node-ról node-ra, preorder módon, ez egy deep copy
{
    Node * newNode = nullptr;
    if(node)
    {
        newNode = new Node(node->getValue());

        newNode->leftChild(cp(node->leftChild(), treep));
        newNode->rightChild(cp(node->rightChild(), treep));

        if(node == treep)
        {
            this->treep = newNode;
        }
    }
    return newNode;
}
public:
BinTree(Node *root = nullptr, Node *treep = nullptr) // a program a Prog1-es LZW fa egy részlete, ez a részlet természetesen nem fordul le csak demonstrációs céllal van itt.
{
this->root = root;              //konstruktor
this->treep = treep;
}
~BinTree()
{
    deltree(root);      //destruktor, törli az egész fát, most a deltree függvény nem lényeges
}
BinTree(const BinTree & old)
{
    root = cp(old.root, old.treep);     //Visszahívás az általunk megírt fa másoló függvényre, itt ezzel nagyon egyszerő ctor-t tudunk írni
}
BinTree & operator=(const BinTree & old)        //copy override, vagyus overridoljuk az operátort
{
    BinTree tmp{old};
    std::swap(*this, tmp);
    return *this;
}
BinTree(BinTree && old)
{
root = nullptr;
*this = std::move(old);     //az std::move() kihasznbálásával könnyen mozgathatjuk az általunk managelt memóriát, természetesen előtte felszabadítjuk az általunk felhasznált memóriát, és biztosra megyünk hogy a destruktor nem fut le kétszer
}
BinTree & operator=(BinTree && old) //move override
{
    std::swap(old.root, root);
    std::swap(old.treep, treep);
    return *this;
}
BinTree & operator<<(T value);  //operator override
#include <iostream>
class Parent
        {
public:
            int asd;
            Parent()
            {
                asd = 10;
            }
            virtual std::string name() {
                return "parent";
            }
        };
class Child : Parent
{
    std::string name() override
    {
        return "child";
    }
    virtual void test()
    {
        Parent parent;
        Child child;        //parentban nem fordulna le mert a Child osztály a parent számára ismeretlen
        std::cout << parent.name();
        std::cout << child.name();
    }
};
int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
package com.company;

public class Main {

    public static void main(String[] args) {
        String first = "...";
        String second = "..."; //cache-elve vannak a rövid stringek ezért a referenciák megegyeznek
        String third = new String("..."); //a "new" kulcsszó használatával garantálom hogy egyedi referencia jön létre

        var firstMatchesSecondWithEquals = first.equals(second); //értéket hasonlít és az megegyezik
        var firstMatchesSecondWithEqualToOperator = first == second; //referenciát hasonlít és az megegyezik
        var firstMatchesThirdWithEquals = first.equals(third); //értéket hasonlít és az egyezik
        var firstMatchesThirdWithEqualToOperator = first == third; //false lesz mert a referencia nem egyezik meg

        System.out.println(firstMatchesSecondWithEquals);
        System.out.println(firstMatchesSecondWithEqualToOperator);
        System.out.println(firstMatchesThirdWithEquals);
        System.out.println(firstMatchesThirdWithEqualToOperator); //ellenőrző print statementek
    }
}

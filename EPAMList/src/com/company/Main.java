package com.company;


import java.util.*;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
	    test2();
    }
    public static void test(List<Double> list, String listname)
    {

        Random random = new Random();

        long start;

        start = System.currentTimeMillis();

        System.out.print("Add 500.00 to "+listname+" \n time:");
        for(int i = 0; i<50000; i++)
            list.add(random.nextGaussian());

        System.out.println(System.currentTimeMillis()-start);

        start = System.currentTimeMillis();
        System.out.print("Add 100.00 to "+listname+" start \n time:");
        for (int i = 0;i<10000;i++)
            list.add(0,random.nextGaussian());
        System.out.println(System.currentTimeMillis()-start);

        start = System.currentTimeMillis();
        System.out.print("Get 50.000 from "+listname+" random \n time:");
        for(int i = 0;i<50000;i++)
            list.get(random.nextInt(50000));
        System.out.println(System.currentTimeMillis()-start);
    }
    public static void test2()
    {
        ArrayList<Double> arrayList = new ArrayList<>();
        LinkedList<Double> linkedList = new LinkedList<>();

        test(arrayList, "ArrayList");
        test(linkedList, "LinkedList");
    }
}

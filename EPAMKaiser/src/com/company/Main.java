package com.company;

import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
//programot erősen ez ihlette: https://www.w3schools.com/java/java_user_input.asp
public class Main {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);       //azért kell a scanner hogy soronként tudjunk olvasni, lényegében figyeli a standard inputot sorvége karakterekért és minden beolvas ami előtte van
        while(in.hasNext()) //amíg sikeres a beolvasás
        {
            kaisar(in.nextLine(), 31);
        }

    }
    public static void kaisar(String line, int shift) throws IOException // a fonetikusság kedvéért #idesofmarch
    {
        FileWriter outputTxt = new FileWriter("kaisar.txt");        //létrehozzuk a fájlt
        for(char c : line.toCharArray())
        {
            outputTxt.write((char) (( c + shift ) % 128)); //fájlba írás
            //a shift-értékével eltoljuk a karaktert.
            //a % azért szükséges hogy körbe érjen
        }
        outputTxt.write("\n");      //endline karakter
        outputTxt.close();  //bezárjuk a fájlt
    }
}

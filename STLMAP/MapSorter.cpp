
#pragma once

#include <map>
#include <iostream>
#include <vector>
#include <algorithm>

template <typename Alpha, typename Beta>
std::vector<std::pair<Alpha, Beta>> sort_map(const std::map<Alpha, Beta>& rank)
{
	std::vector<std::pair<Alpha, Beta>> ordered;

	for (const auto& i : rank)
		ordered.push_back(i);

	std::sort(
		std::begin(ordered),
		std::end(ordered),
		[](const auto& p1, const auto& p2)
	{
		return p1.second < p2.second;
	}
	);

	return ordered;
}

template <typename T>
void sortByValue(std::map<T, T>& map)
{
	std::map<T, T> map2 = std::map<T, T>();

	for (auto i : map)
		map2.insert(std::pair<T, T>(i.second, i.first));

	map = map2;
}

template <typename First, typename Second>
void print(std::map<First, Second>& map)
{
	for (auto elem : map)
		std::cout << elem.first << "\t" << elem.second << "\n";
	std::cout << "\n";
}


int main()
{	//csak teszt
	std::map<int, int> map = std::map<int, int>(); //map deklarsálása

	for (int i = 0; i < 5; i++)
		map.insert(std::pair<int, int>(i, 5 - i));

	for (int i = 5; i < 10; i++)
		map.insert(std::pair<int, int>(i + 5, 10 + 5 - i));

	print(map);

	auto temp = sort_map(map);

	for (const auto& elem : temp)
		std::cout << elem.first << "\t" << elem.second << "\n";
}


package com.company;

import javax.imageio.ImageIO;
import java.awt.*;
import javax.swing.*;
import java.io.IOException;

public class Fullscreen {

    public static void main(String[] args) {
        bigWindow bw = new bigWindow();
    }
}
class bigWindow extends Frame
{
    Image image = null;
    @Override
    public void paint(Graphics g) {                 //overridoljuk a paint functiont hogy kijelezzük a képet
        g.drawImage(image, 600,500,this);
    }
    bigWindow()
    {
        try {
            image = ImageIO.read(getClass().getResource("/resources/doom.png"));        //betöltök egy képet és lekezelem ha nem találja, a jar-ban van a kép az egyszerűség kedvéert
        } catch (IOException asd) {
            asd.printStackTrace();
        }
        setSize(300,300);
        setLayout(null);
        setExtendedState(JFrame.MAXIMIZED_BOTH);    // fullscreen
        setUndecorated(true);   // eltűnik a header
        setVisible(true);   //megjelenítés
    }

}

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse

# Import data
from tensorflow.examples.tutorials.mnist import input_data

old_v = tf.logging.get_verbosity()
tf.logging.set_verbosity(tf.logging.ERROR)

import matplotlib.pyplot
import tensorflow as tf


# szükséges könyvtárak, a tensorflow maga az ML miatt kell, a matplotlib az ábrázolásért
old_v = tf.logging.get_verbosity()
tf.logging.set_verbosity(tf.logging.ERROR)
#logolás mérése: megmondja hány ERROR-t dob

# def readimg():
# file = tf.read_file("sajat8a.png")    #saját kép olvasására is jó lenne
# img = tf.image.decode_png(file)
# return img

def main(_):
    mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)
    #modell elkészítése

    x = tf.placeholder(tf.float32, [None, 784])
    W = tf.Variable(tf.zeros([784, 10]))
    b = tf.Variable(tf.zeros([10]))
    y = tf.matmul(x, W) + b

    #veszteség és optimalizáció megadása

    y_ = tf.placeholder(tf.float32, [None, 10])

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    #hálózat betanítása

    sess = tf.InteractiveSession()
    # Train
    tf.initialize_all_variables().run(session=sess)
    print("-- A halozat tanitasa")
    for i in range(1000):
        batch_xs, batch_ys = mnist.train.next_batch(100)
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
        if i % 100 == 0:
            print(i/10, "%")
    print("----------------------------------------------------------")


    #alapértelmezett session létrehozása
    # a ciklus ezerszer fog lefutni a pontosabb eredmény érdekében
    #ha eléri a 100%-ot, vagyis készen van, akkor elkezdi tesztelni

    print("-- A halozat tesztelese")
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    print("-- Pontossag: ", sess.run(accuracy, feed_dict={x: mnist.test.images,
                                                      y_: mnist.test.labels}))
    print("----------------------------------------------------------")

    print("-- A MNIST 42. tesztkepenek felismerese, mutatom a szamot, a tovabblepeshez csukd be az ablakat")

    img = mnist.test.images[42]
    image = img

    matplotlib.pyplot.imshow(image.reshape(28, 28), cmap=matplotlib.pyplot.cm.binary)
    matplotlib.pyplot.savefig("4.png")
    matplotlib.pyplot.show()

    classification = sess.run(tf.argmax(y, 1), feed_dict={x: [image]})

    print("-- Ezt a halozat ennek ismeri fel: ", classification[0])
    print("----------------------------------------------------------")

    #leteszteljuk a halozat pontossagat
    #megjeleniti a kepet és bezárás után közli hogy milyen számra tippel, aztan a kepet elmenti egy külön fajlba

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--data_dir', type=str, default='/tmp/tensorflow/mnist/input_data',
                      help='Directory for storing input data')
  FLAGS = parser.parse_args()
  tf.app.run()

#az argparser eltárolja a fontos állományokat




package com.company;

public class Main {

    public static void main(String[] args) {
        Vehicle firstVehicle = new Supercar();
        firstVehicle.Start();
        System.out.println(firstVehicle instanceof Car);
        Car secondVehicle = (Car) firstVehicle;
        secondVehicle.Start();
        System.out.println(secondVehicle instanceof Supercar);
        Supercar thirdVehicle = new Vehicle(); //nem fordul le mert a Vehicle a parentje a Supercarnak, az előző példákban pedig a childel inicializáltuk a parentet, mivel a child tartalmaz mindent a parenttől.
        thirdVehicle.Start();
    }
}

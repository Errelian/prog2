package com.company;

public class Supercar extends Car {
    public Supercar()
    {
        System.out.println("Assembling Supercar");
    }

    @Override
    public void Start() {
        System.out.println("Supercar is Starting");
    }
}

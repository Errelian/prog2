package com.company;

public class IntegerTarolo {
    Integer[] adat;
    int count = 0;
    int lastindex = 0;
    int size;

    IntegerTarolo(Integer size)
    {
        adat = new Integer[size];
        this.size = size;
    }
    public void add(Integer number)
    {
        adat[count++] = number;
        lastindex = count-1;
        adat = Sort();
    }
    public boolean Contains(Integer number)
    {
        int start = 0;
        int end = lastindex;

        while (end != start)
        {
            int location = (start + end)/2;
            Integer x = adat[location];
            int asd = x.compareTo(number);
            if (asd == 0)
                return true;
            else if (asd < 0)
                end = location;
            else
                start = location + 1;
        }
        if(adat[start].compareTo(number) == 0)
            return true;
        else
            return false;
    }
    public Integer[] Sort()
    {
        Integer[] temp = adat.clone();

        for (int i = 0; i < count; i++)
        {
            for (int j = i; j < count; j++)
            {
                if (temp[i].compareTo(temp[j]) < 0) {

                    Integer ttemp = temp[i];
                    temp[i] = temp[j];
                    temp[j] = ttemp;
                }
            }
        }
        return temp;
    }
    public Integer[] getadat() {
        return adat;
    }
}
